{-# LANGUAGE OverloadedStrings #-}
import Control.Monad.Trans.Resource
import Data.Conduit (($$))
import Data.Text (Text, unpack)
import Text.XML.Stream.Parse

data Person = Person Int Text
    deriving Show

parsePerson = tagName "person" (requireAttr "age") $ \age -> do
    name <- content
    return $ Person (read $ unpack age) name

parsePeople = tagNoAttr "people" $ many parsePerson

main = do
    people <- runResourceT $
        parseFile def "people.xml" $$ force "people required" parsePeople
    print people